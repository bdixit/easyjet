from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory


def photon_sequence_cfg(flags):
    cfg = ComponentAccumulator()
    from EgammaAnalysisAlgorithms.PhotonAnalysisSequence import (
        makePhotonAnalysisSequence,
    )

    photon_sequence = makePhotonAnalysisSequence(
        flags.Analysis.DataType,
        workingPoint=flags.Analysis.PhotonWP,
        postfix="loose",
        deepCopyOutput=False,
        shallowViewOutput=True,
        crackVeto=False,
        enableCleaning=True,
        cleaningAllowLate=False,
        recomputeIsEM=False,
        ptSelectionOutput=True,
        enableCutflow=False,
        enableKinematicHistograms=False,
    )
    photon_sequence.configure(
        inputName=flags.Analysis.container_names.input.photons,
        outputName=flags.Analysis.container_names.output.photons,
    )

    cfg.addSequence(CompFactory.AthSequencer(photon_sequence.getName()))
    for alg in photon_sequence.getGaudiConfig2Components():
        cfg.addEventAlgo(alg, photon_sequence.getName())

    return cfg
